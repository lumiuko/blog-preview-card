# Frontend Mentor - Blog preview card solution

This is a solution to the [Blog preview card challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/blog-preview-card-ckPaj01IcS). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- See hover and focus states for all interactive elements on the page

### Screenshot

![](https://i.imgur.com/oOzRAMo.png)

### Links

- Solution URL: [https://www.frontendmentor.io/solutions/blog-preview-card-0SO7eR1Do9](https://www.frontendmentor.io/solutions/blog-preview-card-0SO7eR1Do9)
- Live Site URL: [https://blog-preview-page.netlify.app/](https://blog-preview-page.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow

## Author

- Frontend Mentor - [@lumiuko](https://www.frontendmentor.io/profile/lumiuko)
- Twitter - [@lumiuko](https://www.twitter.com/lumiuko)
